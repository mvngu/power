/**************************************************************************
 * Copyright (C) 2013 Minh Van Nguyen <mvngu.name@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * http://www.gnu.org/licenses/
 *************************************************************************/

#include <igraph.h>
#include <stdio.h>

/* The edge list of the C. elegans neural network.  All self-loops are ignored.
 */
int main() {
  FILE *file;
  igraph_t G;          /* the graph from which to obtain degrees */

  file = fopen("powergrid.gml", "r");
  igraph_read_graph_gml(&G, file);
  fclose(file);
  file = fopen("powergrid.edge", "w");
  igraph_write_graph_edgelist(&G, file);
  fclose(file);
  igraph_destroy(&G);

  return 0;
}
